"__author__ = 'Leo Chan and Martin Fiser'"
"__credits__ = 'Keboola 2017'"

"""
Python 3 environment 
"""

import pip
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'dateparser'])
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'boto'])

import csv
import time
from keboola import docker
import boto
from boto.s3.key import Key
from boto.exception import S3ResponseError
import datetime
import dateparser
import pandas as pd
import os
import io
import sys
import requests
import logging
import json
import gzip



# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

# initialize application
cfg = docker.Config('/data/')

# Access the supplied parameters
params = cfg.get_parameters()
KEY_ID = cfg.get_parameters()["keyId"]
SKEY_ID = cfg.get_parameters()["#secret_key"]
BUCKET = cfg.get_parameters()["bucket_source"]
ABBREV = cfg.get_parameters()["area"] #folder_string
FILENAME = cfg.get_parameters()["file_name"] #file_name
include_file_name = cfg.get_parameters()["include_file_name"]
include_row_number = cfg.get_parameters()["include_row_number"]
include_date = cfg.get_parameters()["include_date"]
FILENAME_COLUMN_NAME = cfg.get_parameters()["filename_column_name"]
EXTRACTION_COLUMN_NAME = cfg.get_parameters()["extraction_column_name"]
ROW_COLUMN_NAME = cfg.get_parameters()["row_column_name"]
START_DATE = cfg.get_parameters()["start_date"]
END_DATE = cfg.get_parameters()["end_date"]
COLUMN_HEADER = cfg.get_parameters()["column_header"]
debug = cfg.get_parameters()["debug"]

### Exit if there aren't any column headers
if len(COLUMN_HEADER) == 0:
    logging.error("Please enter column headers.")
    logging.error("Exit")
    sys.exit(0)

if include_file_name == "YES":
    INCLUDE_FILE_NAME = True
else:
    INCLUDE_FILE_NAME = False

if include_row_number == "YES":
    INCLUDE_ROW_NUMBER = True
else:
    INCLUDE_ROW_NUMBER = False

if include_date == "YES":
    INCLUDE_DATE = True
else:
    INCLUDE_DATE = False

if debug == "true":
    DEBUG = True
elif debug == True:
    DEBUG = True
else:
    DEBUG = False

# Get proper list of tables
cfg = docker.Config('/data/')
out_tables = cfg.get_expected_output_tables()
#logging.info("OUT tables mapped: "+str(out_tables))

DEFAULT_TABLE_INPUT = "/data/in/tables/"
DEFAULT_TABLE_OUTPUT = "/data/out/tables/"


class sfile:
    """
    represents files on S3
    """

    def __init__(self, name, size, modified, key):
        self.name = name
        self.size = size
        self.modified = modified
        self.key = key


def get_tables(out_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    table = out_tables[0]
    out_name = table["full_path"]
    out_Destination = table["source"]
    logging.info("Output table: " + str(out_name))
    logging.info("Output table destination: " + str(out_Destination))

    return out_name


def dates_request(start_date, end_date):
    """
    Assemble list of dates based on the settings.
    """

    dates = []
    try:
        start_date_form = dateparser.parse(start_date)
        end_date_form = dateparser.parse(end_date)
        day_diff = (end_date_form-start_date_form).days
        if day_diff < 0:
            logging.error("ERROR: start_date cannot exceed end_date! Exit.")
            sys.exit(0)
        temp_date = start_date_form
        day_n = 0
        
        if day_diff == 0:
            ### normal with leading zeros
            dates.append(temp_date.strftime("%Y-%m-%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
        
        while day_n < day_diff:
            ### normal with leading zeros 
            dates.append(temp_date.strftime("%Y-%m-%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
            temp_date += datetime.timedelta(days=1)
            day_n += 1
            if day_n == day_diff:
                ### normal with leading zeros
                dates.append(temp_date.strftime("%Y-%m-%d"))
                ### no leading zeros
                #dates.append(temp_date.strftime("%Y/%-m/%-d"))
    except TypeError:
        logging.error("ERROR: Please enter valid date parameters! Exit.")
        sys.exit(1)

    return dates


def list_buckets():
    """
    List buckets and their properties
    """

    try:
        buckets = conn.get_all_buckets()
    except (OSError,S3ResponseError) as e:
        logging.error("Could not list buckets. Please check credentials. Exit!")
        sys.exit(0)

    buckets_names = []

    for i in buckets:
        name = i.name
        buckets_names.append(name)

    return buckets_names


def list_items(bucket_name, folder=""):
    """
    list items in the bucket.
    returns a list of objects
    
    supported info:
        .name
        .key
        .size
        .last_modified
    
    if nonexistent is None:
        print "No such bucket!"
    """

    # test if bucket is available
    """
    buckets_available = list_buckets()
    if bucket_name in buckets_available:
        logging.info("Bucket found on S3: "+bucket_name)
        pass
    else:
        logging.error("Bucket is not present on S3. Exit!")
        sys.exit(1)
    """

    bucket = conn.get_bucket(bucket_name, validate=False)
    items = []

    for i in bucket.list(prefix=folder):
        name = i.name
        location = i.key
        size = i.size
        modified = i.last_modified

        info = sfile(name, size, modified, location)
        items.append(info)

    return items


def output_file(data_output, file_output, column_headers):
    """ 
    Output the dataframe input to destination file
    Append to the file if file does not exist
    * row by row
    """

    if not os.path.isfile(file_output):
        with open(file_output, "a") as b:
            data_output.to_csv(b, index=False, columns=column_headers)
        b.close()
    else:
        with open(file_output, "a") as b:
            data_output.to_csv(b, index=False, header=False, columns=column_headers)
        b.close()

    return


def main():
    """
    Main execution script.
    """

    bucket_name = BUCKET
    folder_string = ABBREV
    OUTPUT_FILE = get_tables(out_tables) 

    """ Generate a list of dates to download based on dates request """
    NOW = dateparser.parse("today").strftime("%Y-%m-%d %H:%M:%S %z")
    dates = dates_request(START_DATE, END_DATE)

    """ Find out which files need to be outputed """
    logging.info("Getting file list.")
    itemslist = list_items(bucket_name, folder_string)
    full_list = []

    bad_dates = []
    date_format = "YYYY-MM-DD"
    """
    part_1 = FILENAME.split("*")[0]
    part_2 = FILENAME.split("*")[1]
    print(part_1)
    print(part_2)
    sys.exit(0)
    """

    for j in dates:
        string = FILENAME.replace(date_format, str(j))
        string_1 = string.split("*")[0]
        string_2 = string.split("*")[1]
        string_3 = string.split("*")[2]
        bool_exist = False
        for i in itemslist:
            if (string_1 in (i.name)) and (string_2 in (i.name)):
                full_list.append(i.name)
                bool_exist = True
        if bool_exist==False:
            #logging.info("{0} does not exist.".format(string))
            logging.info("{0} does not contain any data.".format(string))
            bad_dates.append(j)
    
    """ Output by files """
    output_df = pd.DataFrame()
    good_output = []
    bad_output = []

    logging.info("File count: "+str(len(full_list)))
    logging.info("Starting file downloads.")

    for i in full_list:
        file_source = i
        #logging.info("file source: {0}".format(file_source))
        file_name = i.split(".csv")[0]
        file_input = DEFAULT_TABLE_INPUT+file_name+".gz"
        #logging.info("file_output destination: {0}".format(file_input))

        bucket = conn.get_bucket(bucket_name, validate=False)
        k = Key(bucket,file_source)
        
        try:
            k.get_contents_to_filename(file_input)

            """ Reading the source file as DF"""
            if DEBUG == True:
                logging.info(file_source+" downloaded.")
            data = pd.read_table(file_input, compression="zip", dtype=str, sep=",", header=None)
            data.columns = COLUMN_HEADER
            column_header = []
            for i in COLUMN_HEADER:
                column_header.append(i)
            if INCLUDE_FILE_NAME:
                data[str(FILENAME_COLUMN_NAME)] = file_source
                column_header.append(str(FILENAME_COLUMN_NAME))
                """
                if FILENAME_COLUMN_NAME not in column_header:
                    column_header.append(str(FILENAME_COLUMN_NAME))
                else:
                    pass
                """
            if INCLUDE_ROW_NUMBER:
                data[str(ROW_COLUMN_NAME)] = data.index.map(int).astype(int)+1
                column_header.append(str(ROW_COLUMN_NAME))
                """
                if ROW_COLUMN_NAME not in column_header:
                    column_header.append(str(ROW_COLUMN_NAME))
                else:
                    pass
                """

            if INCLUDE_DATE:
                data[str(EXTRACTION_COLUMN_NAME)] = NOW
                column_header.append(str(EXTRACTION_COLUMN_NAME))
                """
                if EXTRACTION_COLUMN_NAME not in column_header:
                    column_header.append(str(EXTRACTION_COLUMN_NAME))
                else:
                    pass
                """

            """ Output """
            file_output = file_name+".csv"
            #logging.info("Output: {0} to {1}".format(file_output, OUTPUT_FILE))

            """ Append the modified data to the output table """
            output_file(data, OUTPUT_FILE, column_header)
            del data
            good_output.append(i)      
        except S3ResponseError:
            logging.error("ERROR: Could not retrieve {0}. Check storage class?".format(i))
        except Exception as b:
            logging.error("ERROR: {0} - {1}".format(i,b))
            bad_output.append(i)

    return



if __name__ == "__main__":
    try:
        conn = boto.connect_s3(KEY_ID,SKEY_ID)
        logging.info("Successfully connected."+str(conn))
    except Exception as a:
        logging.error("Could not connect. Exit!")
        sys.exit(1)

    main()

    logging.info("Done.")
